import React, { Suspense, useState, useEffect } from "react";
import "./App.css";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import LayoutManagement from "./UIcomponents/LayoutManagement";
import ProductsManagement from "./UIcomponents/viewManagement/CategoriesProductsView";
import AuctionManagement from "./UIcomponents/viewManagement/AuctionView";
import ManageGoodsReceiptComponents from "./pages/admin/goodsreceipt";
import FormPage from "./pages/login";
import ManageInventoryComponents from "./pages/admin/inventory";
import WrappedRegistrationForm from "./pages/register/index";
import Layout from "./UIcomponents/Layout";
import HomeView from "./UIcomponents/viewAuctioneer/HomeView";
import AuctionView from "./UIcomponents/viewAuctioneer/AuctionView";

function App() {
  const [Author, setAuthor] = useState(window.localStorage.getItem("username"));
  const [Authen, setAuthen] = useState(window.localStorage.getItem("role"));
  useEffect(() => {
    setAuthor(window.localStorage.getItem("username"));
    setAuthen(window.localStorage.getItem("role"));
  }, []);
  console.log(Author);
  return (
    <Suspense>
      <Router>
        <Switch>
          {/* Config router như kiểu dưới.. để khỏi render nhưng thứ k cần render lại */}
          {/* <Redirect to="/" /> */}
          {Author ? (
            <>
              {Authen === "1" ? (
                <>
                  <>
                    <Route
                      exact
                      key="admin"
                      path="/admin"
                      component={() => {
                        return (
                          <>
                            <LayoutManagement>
                              <ProductsManagement />
                            </LayoutManagement>
                          </>
                        );
                      }}
                    ></Route>
                  </>
                  <Route
                    exact
                    key="admin-categoriesandproducts"
                    path="/admin/categories-products"
                    component={() => {
                      return (
                        <>
                          <LayoutManagement>
                            <ProductsManagement />
                          </LayoutManagement>
                        </>
                      );
                    }}
                  />
                  <Route
                    exact
                    key="admin-auction"
                    path="/admin/auction"
                    component={() => {
                      return (
                        <>
                          <LayoutManagement>
                            <AuctionManagement />
                          </LayoutManagement>
                        </>
                      );
                    }}
                  />
                  <Route
                    exact
                    key="admin-goodreceipt"
                    path="/admin/goodsreceipt"
                    component={() => {
                      return (
                        <>
                          <LayoutManagement>
                            <ManageGoodsReceiptComponents />
                          </LayoutManagement>
                        </>
                      );
                    }}
                  />
                  <Route
                    exact
                    key="admin-inventory"
                    path="/admin/inventory"
                    component={() => {
                      return (
                        <>
                          <LayoutManagement>
                            <ManageInventoryComponents />
                          </LayoutManagement>
                        </>
                      );
                    }}
                  />
                </>
              ) : (
                  <>
                  </>
                )}
            </>
          ) : (
              <>
                <Route
                  exact
                  key="home"
                  path="/login"
                  component={() => {
                    return (
                      <>
                        <div
                          style={{
                            display: "block",
                            width: "30vh",
                            margin: "auto",
                            marginTop: "328px",
                          }}
                        >
                          <FormPage setAuthor={setAuthor} setAuthen={setAuthen} />
                        </div>
                      </>
                    );
                  }}
                ></Route>
                <Route
                  exact
                  key="register"
                  path="/register"
                  component={() => {
                    return (
                      <>
                        <div
                          style={{
                            display: "block",
                            width: "30vh",
                            margin: "auto",
                          }}
                        >
                          <WrappedRegistrationForm />
                        </div>
                      </>
                    );
                  }}
                ></Route>
              </>
            )}
        </Switch>
        <Route
          exact
          key="productAutions"
          path="/productAutions"
          component={() => {
            return (
              <>
                <Layout Author={Author}>
                  <HomeView />
                </Layout>
              </>
            );
          }}
        />
        <Route
          exact
          key="productAutions"
          path="/"
          component={() => {
            return (
              <>
                <Layout Author={Author}>
                  <HomeView />
                </Layout>
              </>
            );
          }}
        />
        <Route
          exact
          key="aution"
          path="/auction/:id"
          component={() => {
            return (
              <>
                <Layout Author={Author}>
                  <AuctionView />
                </Layout>
              </>
            );
          }}
        />
      </Router>
    </Suspense>
  );
}

export default App;
