import React, { useEffect } from 'react';
import { useState } from 'react';
import axios from "axios";
import ProductsAuctionModel from './ProductsAuctionModel';
import { API } from '../../../setting';

const ProductsAuctionView = (props) => {
    const [data, setData] = useState([]);
    const fetchData = () => {
        axios.get(API + "/api/productAuctions").then((res) => {
          console.log(res.data);
          setData(
            res.data.map((ele, index) => {
              return {
                ...ele,
                key: index,
                categoryName: ele?.product?.category?.categoryName,
                image: ele?.product?.image,
                productName: ele?.product?.productName,
              };
            })
          );
        });
      };
      useEffect(() => {
        fetchData();
      }, []);

    return (
        <div>
            {data.map(Item => <ProductsAuctionModel item = {Item}/>)}
        </div>
    );
}

export default ProductsAuctionView;
