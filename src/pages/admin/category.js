import React, { useState, useEffect } from "react";
import {
  Table,
  Button,
  Input,
  notification,
  Popconfirm,
} from "antd";
import axios from "axios";
import moment from "moment";
import { API } from "../../setting";
import { PlusOutlined, EditOutlined, DeleteOutlined } from "@ant-design/icons";
import Modal from "antd/lib/modal/Modal";

const ManageCategoriesComponents = () => {
  const [data, setData] = useState([]);
  const [visible, setVisible] = useState(false);
  const [dataSelection, setDataSelection] = useState();
  const fetchData = () => {
    axios.get(API + "/api/categories").then((res) => {
      console.log(res.data);
      setData(
        res.data.map((ele, index) => {
          return {
            ...ele,
            key: index,
          };
        })
      );
    });
  };
  useEffect(() => {
    fetchData();
  }, []);
  const [rowSelectKey, setRowSelectKey] = useState([]) 
  console.log(rowSelectKey)
  const rowSelection = {
    selectedRowKeys:  rowSelectKey,
    onChange: (selectedRowKeys, selectedRows) => {
      // setValue()
      setDataSelection(selectedRows[0]);
      const a = selectedRows[0].categoryName;
      setValue(a);
      setRowSelectKey([...selectedRowKeys])
    },
    getCheckboxProps: (record) => ({
      disabled: record.name === "Disabled User", // Column configuration not to be checked
      name: record.name,
    }),
  };
  const columns = [
    {
      title: "ID",
      dataIndex: "id",
    },
    {
      title: "Name",
      dataIndex: "categoryName",
      width: "500px",
    },
    {
      title: "Created At",
      dataIndex: "createdAt",
      render: (text) => moment(text).format("DD MM YYYY hh:mm:ss a"),
    },
  ];
  useEffect(() => {
    if (!visible) {
      setValue("");
    }
  }, [visible]);
  const [value, setValue] = useState("");
  const handkeOk = () => {
    if (!dataSelection) {
      axios
        .post(API + "/api/categories", {
          categoryName: value,
          isActive: true
        })
        .then((res) => {
          if (res.status === 200) {
            notification["success"]({
              message: "Success",
              description: "Add new category success",
            });
            setVisible(false);
            fetchData();
          } else {
            notification["error"]({
              type: "error",
              content: "Add failed",
            });
            setVisible(false);
          }
        });
    } else {
      axios
        .put(API + "/api/categories/" + dataSelection.id, {
          categoryName: value,
          isActive: true
        })
        .then((res) => {
          if (res.status === 200) {
            notification["success"]({
              message: "Success",
              description: "Update category success",
            });
            setVisible(false);
            fetchData();
          } else {
            notification["error"]({
              type: "error",
              content: "Delete failed",
            });
            setVisible(false);
          }
        });
    }
    setDataSelection()
    setRowSelectKey([])
  };
  const deleteOk = () => {
    axios.delete(API + "/api/categories/" + dataSelection.id).then((res) => {
      if (res.status === 200) {
        notification["success"]({
          message: "Success",
          description: "Delete category success",
        });
        setVisible(false);
        fetchData();
      } else {
        notification["error"]({
          type: "error",
          content: "Delete failed",
        });
        setVisible(false);
      }
    });
    setDataSelection()
    setRowSelectKey([])
  };
  return (
    <div>
      <Modal
        title={!dataSelection ? "Add new category" : "Edit category"}
        visible={visible}
        onOk={() => handkeOk()}
        onCancel={() => setVisible(false)}
      >
        <Input value={value} onChange={(e) => setValue(e.target.value)} />
      </Modal>
      <div style={{ display: "flex", justifyContent: "flex-end" }}>
        <Button
          onClick={() => {
            setVisible(true);
            setDataSelection();
          }}
        >
          <PlusOutlined />
        </Button>
        <Button onClick={() => setVisible(true)} disabled={!dataSelection}>
          <EditOutlined />
        </Button>
        <Popconfirm title="Sure to delete?" onConfirm={() => deleteOk()}>
          <Button disabled={!dataSelection}>
            <DeleteOutlined />
          </Button>
        </Popconfirm>
      </div>
      <Table
        rowSelection={{
          type: "radio",
          ...rowSelection,
        }}
        columns={columns}
        // onChange={(value) => console.log(value)}
        dataSource={data}
      />
      
    </div>
  );
};

export default ManageCategoriesComponents;
