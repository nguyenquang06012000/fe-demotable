import React, { useEffect, useState } from "react";
import { Modal, Form, Input, Radio, Select, Upload, Button } from "antd";
import axios from "axios";
import { API } from "../../../setting";
import { PlusOutlined } from "@ant-design/icons";

const ModalFormComponent = ({
  visible,
  onCancel,
  onOk: onCreate,
  form,
  dataSelection,
  setURI,
}) => {
  const [dataCategories, setDataCategories] = useState([]);
  const fetchDataCategories = () => {
    axios.get(API + "/api/categories").then((res) => {
      console.log(res)
      setDataCategories(
        res.data.map((ele, index) => {
          return {
            ...ele,
            key: index,
          };
        })
      );
    });
  };
  useEffect(() => {
    if (visible) {
      fetchDataCategories();
    }
  }, [visible]);
  useEffect(() => {
    if (dataSelection) {
      setURI(dataSelection.imageName);
    }
  }, [dataSelection]);
  const { getFieldDecorator } = form;
  const { Option } = Select;
  const changeImage = (info) => {
    console.log(info.file);
    if (info?.file?.response) {
      setURI(info.file?.response[0]?.fileName);
    }
  };
  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
    },
  };
  return (
    <Modal
      visible={visible}
      title={!dataSelection ? "Add new product" : "Edit product"}
      okText="Submit"
      onCancel={onCancel}
      onOk={onCreate}
      width='50%'
    >
      <Form {...formItemLayout}>
        <Form.Item label="Category">
          {getFieldDecorator("idCategory", {
            initialValue: dataSelection?.category?.id || "",
            rules: [
              {
                required: true,
                message: "Please choose a category!",
              },
            ],
          })(
            <Select>
              {dataCategories.map((ele) => {
                return (
                  <Option value={ele.id} value={ele.id}>
                    {" "}
                    {ele.categoryName}{" "}
                  </Option>
                );
              })}
            </Select>
          )}
        </Form.Item>
        <Form.Item label="Name product">
          {getFieldDecorator("productName", {
            initialValue: dataSelection?.productName || "",
            rules: [
              {
                required: true,
                message: "Please input name product",
              },
            ],
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Description">
          {getFieldDecorator("description", {
            initialValue: dataSelection?.description || "",
          })(<Input type="textarea" />)}
        </Form.Item>
        <Form.Item label="Image product">
          {getFieldDecorator("image", {
            initialValue: dataSelection?.image || "",
            rules: [
              {
                required: true,
                message: "Please choose a image!",
              },
            ],
          })(
            <Upload
              action={`${API}/api/uploadFile`}
              onChange={(Info) => changeImage(Info)}
              defaultFileList={
                dataSelection
                  ? [
                      {
                        uid: "-4",
                        name: dataSelection.imageName + ".png",
                        status: "done",
                        src: dataSelection?.image,
                      },
                    ]
                  : []
              }
            >
              <Button>
                <PlusOutlined />
              </Button>
            </Upload>
          )}
        </Form.Item>
      </Form>
    </Modal>
  );
};

const ModalForm = Form.create({ name: "modal_form" })(ModalFormComponent);

export default ModalForm;
