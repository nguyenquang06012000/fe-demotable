import React, { useState, useEffect, useCallback } from "react";
import { Table, Divider, Button, notification, Popconfirm, Select } from "antd";
import axios from "axios";
import moment from "moment";
import { PlusOutlined, EditOutlined, DeleteOutlined } from "@ant-design/icons";
import ModalForm from "./modalProduct";
import { API } from "../../../setting";

const ManageProductsComponents = (props) => {
  const [data, setData] = useState([]);
  const [visible, setVisible] = useState(false);
  const [dataSelection, setDataSelection] = useState();
  const [formRef, setFormRef] = useState(null);
  const [rowSelectKey, setRowSelectKey] = useState([]) 
  const [URIimage, setURI] = useState("");
  const fetchData = () => {
    axios.get(API + "/api/products").then((res) => {
      console.log(res.data);
      setData(
        res.data.map((ele, index) => {
          return {
            ...ele,
            key: index,
            categoryName: ele.category.categoryName,
          };
        })
      );
    });
  };
  useEffect(() => {
    fetchData();
  }, []);
  const rowSelection = {
    selectedRowKeys:  rowSelectKey,
    onChange: (selectedRowKeys, selectedRows) => {
      setDataSelection(selectedRows[0]);
      setRowSelectKey([...selectedRowKeys])
    },
    getCheckboxProps: (record) => ({
      disabled: record.name === "Disabled User", // Column configuration not to be checked
      name: record.name,
    }),
  };
  const columns = [
    {
      title: "ID",
      dataIndex: "id",
    },
    {
      title: "Name",
      dataIndex: "productName",
      width: "450px",
    },
    {
      title: "Image",
      render: (theImageURL) => (
        <div style={{ height: "100px", width: "100px" }}>
          <img
            alt={theImageURL}
            src={theImageURL}
            style={{ height: "100px", width: "100px" }}
          />
        </div>
      ),
      dataIndex: "image",
    },
    {
      title: "Category",
      dataIndex: "categoryName",
    },
    {
      title: "Description",
      dataIndex: "description",
    },
    {
      title: "createdAt",
      dataIndex: "createdAt",
      render: (text) => moment(text).format("DD MM YYYY hh:mm:ss a"),
    },
  ];
  const saveFormRef = useCallback((node) => {
    if (node) {
      setFormRef(node);
    }
  }, []);
  const handleOk = () => {
    formRef.validateFields((err, values) => {
      console.log(values);
      if (URIimage) {
        if (!dataSelection) {
          axios
            .post(API + "/api/products", {
              ...values,
              category: {
                id: values.idCategory,
              },
              image: URIimage,
            })
            .then((res) => {
              if (res.status === 200) {
                notification["success"]({
                  message: "Success",
                  description: "Add new product success",
                });
                setVisible(false);
                fetchData();
              } else {
                notification["error"]({
                  type: "error",
                  content: "Add failed",
                });
                setVisible(false);
              }
            });
        } else {
          axios
            .put(API + "/api/products/" + dataSelection.id, {
              ...values,
              category: {
                id: values.idCategory,
              },
              image: URIimage,
            })
            .then((res) => {
              if (res.status === 200) {
                notification["success"]({
                  message: "Success",
                  description: "Update product success",
                });
                setVisible(false);
                fetchData();
              } else {
                notification["error"]({
                  type: "error",
                  content: "Delete failed",
                });
                setVisible(false);
              }
            });
        }
      }
    });
    setDataSelection();
    setRowSelectKey([])
  };
  const deleteOk = () => {
    axios.delete(API + "/api/products/" + dataSelection.id).then((res) => {
      if (res.status === 200) {
        notification["success"]({
          message: "Success",
          description: "Delete product success",
        });
        setVisible(false);
        fetchData();
      } else {
        notification["error"]({
          type: "error",
          content: "Delete failed",
        });
        setVisible(false);
      }
    });
    setDataSelection();
    setRowSelectKey([])
  };
  return (
    <div>
      <ModalForm
        ref={saveFormRef}
        visible={visible}
        onCancel={() => setVisible(false)}
        onOk={() => handleOk()}
        dataSelection={dataSelection}
        URIimage={URIimage}
        setURI={setURI}
      />
      <div style={{ display: "flex", justifyContent: "flex-end" }}>
        <Button
          onClick={() => {
            setVisible(true);
            setDataSelection();
          }}
        >
          <PlusOutlined />
        </Button>
        <Button onClick={() => setVisible(true)} disabled={!dataSelection}>
          <EditOutlined />
        </Button>
        <Popconfirm title="Sure to delete?" onConfirm={() => deleteOk()}>
          <Button disabled={!dataSelection}>
            <DeleteOutlined />
          </Button>
        </Popconfirm>
      </div>
      <Table
        rowSelection={{
          type: "radio",
          ...rowSelection,
        }}
        columns={columns}
        dataSource={data}
      />
    </div>
  );
};

export default ManageProductsComponents;
