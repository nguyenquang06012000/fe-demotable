import React, { useState, useEffect, useCallback } from "react";
import { Table, Divider, Button, notification, Popconfirm, Select } from "antd";
import axios from "axios";
import moment from "moment";
import { PlusOutlined, EditOutlined, DeleteOutlined } from "@ant-design/icons";
// import ModalForm from "./modalForm";
import { API } from "../../../setting";
import ModalForm from "./modalForm";

const ProducAuctionsComponents = (props) => {
  const [data, setData] = useState([]);
  const [rowSelectKey, setRowSelectKey] = useState([]) 
  const [visible, setVisible] = useState(false);
  const [dataSelection, setDataSelection] = useState();
  const [formRef, setFormRef] = useState(null);
  const [URIimage, setURI] = useState("");
  const fetchData = () => {
    axios.get(API + "/api/productAuctions").then((res) => {
      console.log(res.data);
      setData(
        res.data.map((ele, index) => {
          return {
            ...ele,
            key: index,
            categoryName: ele?.product?.category?.categoryName,
            image: ele?.product?.image,
            productName: ele?.product?.productName,
          };
        })
      );
    });
  };
  useEffect(() => {
    fetchData();
  }, []);
  const rowSelection = {
    selectedRowKeys:  rowSelectKey,
    onChange: (selectedRowKeys, selectedRows) => {
      setDataSelection(selectedRows[0]);
      setRowSelectKey([...selectedRowKeys])
    },
    getCheckboxProps: (record) => ({
      disabled: record.name === "Disabled User", // Column configuration not to be checked
      name: record.name,
    }),
  };
  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      hide: true,
    },
    {
      title: "Name",
      dataIndex: "productName",
      width: "300px",
    },
    {
      title: "Description",
      dataIndex: "description",
    },
    {
      title: "Status",
      dataIndex: "status",
      render: (value) => {
        if (value) return "Sold";
        return "Not sold yet";
      },
    },
    {
      title: "Image",
      render: (theImageURL) => (
        <div style={{ height: "100px", width: "100px" }}>
          <img
            alt={theImageURL}
            src={theImageURL}
            style={{ height: "100px", width: "100px" }}
          />
        </div>
      ),
      dataIndex: "image",
    },
    {
      title: "Category",
      dataIndex: "categoryName",
    },
    {
      title: "startDate",
      dataIndex: "startDate",
      render: (text) => moment(text).format("DD MM YYYY hh:mm:ss a"),
    },
    {
      title: "endDate",
      dataIndex: "endDate",
      render: (text) => moment(text).format("DD MM YYYY hh:mm:ss a"),
    },
    {
      title: "minPrice",
      dataIndex: "minPrice",
    },
    {
      title: "Incremenent",
      dataIndex: "incremenent",
    },
    {
      title: "createdAt",
      dataIndex: "createdAt",
      render: (text) => moment(text).format("DD MM YYYY hh:mm:ss a  "),
    },
  ];
  const saveFormRef = useCallback((node) => {
    if (node) {
      setFormRef(node);
    }
  }, []);
  const handleOk = () => {
    formRef.validateFields((err, values) => {
      const startDate =
        moment(values?.dateStart).format("L") +
        " " +
        moment(values?.timeStart).format("LTS");
      const endDate =
        moment(values?.dateEnd).format("L") +
        " " +
        moment(values?.timeEnd).format("LTS");
      delete values.timeStart;
      delete values.timeEnd;
      delete values.dateStart;
      delete values.dateEnd;
      const input = {
        ...values,
        startDate,
        endDate,
        status: false,
        incremenent: dataSelection?.incremenent,
      };
      input.isActive = true;
      if (!dataSelection) {
        input.isActive = true;
        input.incremenent = 0;
        axios.post(API + "/api/productAuctions", input).then((res) => {
          if (res.status === 200) {
            notification["success"]({
              message: "Success",
              description: "Add new product auction success",
            });
            setVisible(false);
            fetchData();
          } else {
            notification["error"]({
              type: "error",
              content: "Add failed",
            });
            setVisible(false);
          }
        });
      } else {
        axios
          .put(API + "/api/productAuctions/" + dataSelection.id, input)
          .then((res) => {
            if (res.status === 200) {
              notification["success"]({
                message: "Success",
                description: "Update product aucction success",
              });
              setVisible(false);
              fetchData();
            } else {
              notification["error"]({
                type: "error",
                content: "Update failed",
              });
              setVisible(false);
            }
          });
      }
    });
    setDataSelection()
    setRowSelectKey([])
  };
  const deleteOk = () => {
    axios
      .delete(API + "/api/productAuctions/" + dataSelection.id)
      .then((res) => {
        if (res.status === 200) {
          notification["success"]({
            message: "Success",
            description: "Delete product aution success",
          });
          setVisible(false);
          fetchData();
        } else {
          notification["error"]({
            type: "error",
            content: "Delete failed",
          });
          setVisible(false);
        }
      });
      setDataSelection()
      setRowSelectKey([])
  };
  return (
    <div>
      <ModalForm
        ref={saveFormRef}
        visible={visible}
        onCancel={() => setVisible(false)}
        onOk={() => handleOk()}
        dataSelection={dataSelection}
        URIimage={URIimage}
        setURI={setURI}
      />
      <div style={{ display: "flex", justifyContent: "flex-end" }}>
        <Button
          onClick={() => {
            setVisible(true);
            setDataSelection();
          }}
        >
          <PlusOutlined />
        </Button>
        <Button onClick={() => setVisible(true)} disabled={!dataSelection}>
          <EditOutlined />
        </Button>
        <Popconfirm title="Sure to delete?" onConfirm={() => deleteOk()}>
          <Button disabled={!dataSelection}>
            <DeleteOutlined />
          </Button>
        </Popconfirm>
      </div>
      <Table
        rowSelection={{
          type: "radio",
          ...rowSelection,
        }}
        columns={columns}
        dataSource={data}
      />
    </div>
  );
};

export default ProducAuctionsComponents;
