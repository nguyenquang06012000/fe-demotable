import React, { useEffect, useState } from "react";
import { Modal, Form, Input, Select, DatePicker, TimePicker } from "antd";
import axios from "axios";
import moment from "moment";
import { API } from "../../../setting";

const ModalFormComponent = ({
  visible,
  onCancel,
  onOk: onCreate,
  form,
  dataSelection,
  setURI,
}) => {
  const [dataCategories, setDataCategories] = useState([]);
  const fetchDataCategories = () => {
    axios.get(API + "/api/products").then((res) => {
      setDataCategories(
        res.data.map((ele, index) => {
          return {
            ...ele,
            key: index,
          };
        })
      );
    });
  };
  useEffect(() => {
    fetchDataCategories();
  }, []);
  useEffect(() => {
    if (dataSelection) {
      setURI(dataSelection.imageName);
    }
  }, [dataSelection]);
  const { getFieldDecorator } = form;
  const { Option } = Select;
  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
    },
  };
  return (
    <Modal
      visible={visible}
      title={!dataSelection ? "New product action" : "Edit product aution"}
      okText="Submit"
      onCancel={onCancel}
      onOk={onCreate}
      width='50%'
    >
      <Form {...formItemLayout}>
        <Form.Item label="Product" >
          {getFieldDecorator("idProduct", {
            initialValue: dataSelection?.product?.id || "",
            rules: [
              {
                required: true,
                message: "Please choose a product!",
              },
            ],
          })(
            <Select
              disabled={!!dataSelection}
            >
              {dataCategories.map((ele) => {
                return (
                  <Option value={ele.id} value={ele.id}>
                    {" "}
                    {ele.productName}{" "}
                  </Option>
                );
              })}
            </Select>
          )}
        </Form.Item>
        <Form.Item label="minPrice">
          {getFieldDecorator("minPrice", {
            initialValue: dataSelection?.minPrice || 0,
          })(<Input type="number" />)}
        </Form.Item>
        <Form.Item label="Start Date">
          <div style={{ display: "flex" }}>
            {getFieldDecorator("dateStart", {
              initialValue: moment(dataSelection?.startDate || new Date()),
            })(<DatePicker />)}
            {getFieldDecorator("timeStart", {
             initialValue: moment(dataSelection?.startDate || new Date()),
            })(<TimePicker />)}
          </div>
        </Form.Item>
        <Form.Item label="End Date">
          <div style={{ display: "flex" }}>
            {getFieldDecorator("dateEnd", {
              initialValue: moment(dataSelection?.endDate || new Date()),
            })(<DatePicker />)}
            {getFieldDecorator("timeEnd", {
              initialValue: moment(dataSelection?.endDate || new Date()),
            })(<TimePicker />)}
          </div>
        </Form.Item>
        <Form.Item label="Description">
          {getFieldDecorator("description", {
            initialValue: dataSelection?.description || "",
          })(<Input />)}
        </Form.Item>
      </Form>
    </Modal>
  );
};

const ModalForm = Form.create({ name: "modal_form" })(ModalFormComponent);

export default ModalForm;
