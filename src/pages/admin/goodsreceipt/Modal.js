import React, { useEffect, useState } from "react";
import { Modal, Form, Input, Select, DatePicker, TimePicker } from "antd";
import axios from "axios";
import moment from "moment";
import { API } from "../../../setting";

const ModalFormComponent = ({
  visible,
  onCancel,
  onOk: onCreate,
  form,
  dataSelection,
}) => {
  const [dataProducts, setdataProducts] = useState([]);
  const fetchDataProducts = () => {
    axios.get(API + "/api/products").then((res) => {
      setdataProducts(
        res.data.map((ele, index) => {
          return {
            ...ele,
            key: index,
          };
        })
      );
    });
  };
  useEffect(() => {
    fetchDataProducts();
  }, []);
  const { getFieldDecorator } = form;
  const { Option } = Select;
  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
    },
  };
  return (
    <Modal
      visible={visible}
      title={!dataSelection ? "Store Import" : "Edit"}
      okText="Submit"
      onCancel={onCancel}
      onOk={onCreate}
      width='50%'
    >
      <Form {...formItemLayout}>
        <Form.Item label="Product" >
          {getFieldDecorator("idProduct", {
            initialValue: dataSelection?.product?.id || "",
            rules: [
              {
                required: true,
                message: "Please choose a product!",
              },
            ],
          })(
            <Select
              disabled={!!dataSelection}
            >
              {dataProducts.map((ele) => {
                return (
                  <Option value={ele.id} value={ele.id}>
                    {" "}
                    {ele.productName}{" "}
                  </Option>
                );
              })}
            </Select>
          )}
        </Form.Item>
        <Form.Item label="Quantity">
          {getFieldDecorator("quantity", {
            initialValue: dataSelection?.quantity || 0,
          })(<Input type="number" />)}
        </Form.Item>
        <Form.Item label="Price Each">
          {getFieldDecorator("priceEach", {
            initialValue: dataSelection?.priceEach || 0,
          })(<Input type="number" />)}  
        </Form.Item>
      </Form>
    </Modal>
  );
};

const ModalForm = Form.create({ name: "modal_form" })(ModalFormComponent);

export default ModalForm;
