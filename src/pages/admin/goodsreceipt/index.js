import React, { useState, useEffect, useCallback } from "react";
import { Table, Button, notification, Popconfirm } from "antd";
import axios from "axios";
import moment from "moment";
import { PlusOutlined, EditOutlined, DeleteOutlined } from "@ant-design/icons";
import { API } from "../../../setting";
import ModalForm from "./Modal";

const ManageGoodsReceiptComponents = (props) => {
  const [data, setData] = useState([]);
  const [visible, setVisible] = useState(false);
  const [dataSelection, setDataSelection] = useState();
  const [rowSelectKey, setRowSelectKey] = useState([]);
  const [formRef, setFormRef] = useState(null);
  const [URIimage, setURI] = useState("");
  const fetchData = () => {
    axios.get(API + "/api/goodsResceipts").then((res) => {
      console.log(res.data);
      setData(
        res.data.map((ele, index) => {
          return {
            ...ele,
            key: index,
            productName: ele.product.productName,
          };
        })
      );
    });
  };
  useEffect(() => {
    fetchData();
  }, []);
  const rowSelection = {
    selectedRowKeys: rowSelectKey,
    onChange: (selectedRowKeys, selectedRows) => {
      setDataSelection(selectedRows[0]);
      setRowSelectKey([...selectedRowKeys]);
    },
    getCheckboxProps: (record) => ({
      disabled: record.name === "Disabled User", // Column configuration not to be checked
      name: record.name,
    }),
  };
  const columns = [
    {
      title: "ID",
      dataIndex: "id",
    },
    {
      title: "Name Product",
      dataIndex: "productName",
      width: "450px",
    },
    {
      title: "Quantity",
      dataIndex: "quantity",
    },
    {
      title: "Price Each",
      dataIndex: "priceEach",
    },
    {
      title: "createdAt",
      dataIndex: "createdAt",
      render: (text) => moment(text).format("DD MM YYYY hh:mm:ss a"),
    },
  ];
  const saveFormRef = useCallback((node) => {
    if (node) {
      setFormRef(node);
    }
  }, []);
  const handleOk = () => {
    formRef.validateFields((err, values) => {
      values.isActive = true;
      let input = values;
      if (!dataSelection) {
        axios.post(API + "/api/goodsResceipts", input).then((res) => {
          if (res.status === 200) {
            notification["success"]({
              message: "Success",
              description: "Import success",
            });
            setVisible(false);
            fetchData();
          } else {
            notification["error"]({
              type: "error",
              content: "Failed",
            });
            setVisible(false);
          }
        });
      } else {
        axios
          .put(API + "/api/goodsResceipts/" + dataSelection.id, input)
          .then((res) => {
            if (res.status === 200) {
              notification["success"]({
                message: "Success",
                description: "Update success",
              });
              setVisible(false);
              fetchData();
            } else {
              notification["error"]({
                type: "error",
                content: "Update failed",
              });
              setVisible(false);
            }
          });
      }
    });
    setDataSelection();
    setRowSelectKey([]);
  };
  const deleteOk = () => {
    axios
      .delete(API + "/api/productAuctions/" + dataSelection.id)
      .then((res) => {
        if (res.status === 200) {
          notification["success"]({
            message: "Success",
            description: "Delete product aution success",
          });
          setVisible(false);
          fetchData();
        } else {
          notification["error"]({
            type: "error",
            content: "Delete failed",
          });
          setVisible(false);
        }
      });
    setDataSelection();
    setRowSelectKey([]);
  };
  return (
    <div>
      <ModalForm
        ref={saveFormRef}
        visible={visible}
        onCancel={() => setVisible(false)}
        onOk={() => handleOk()}
        dataSelection={dataSelection}
        URIimage={URIimage}
        setURI={setURI}
      />
      <div style={{ display: "flex", justifyContent: "flex-end" }}>
        <Button
          onClick={() => {
            setVisible(true);
            setDataSelection();
          }}
        >
          <PlusOutlined />
        </Button>
        <Button onClick={() => setVisible(true)} disabled={!dataSelection}>
          <EditOutlined />
        </Button>
        <Popconfirm title="Sure to delete?" onConfirm={() => deleteOk()}>
          <Button disabled={!dataSelection}>
            <DeleteOutlined />
          </Button>
        </Popconfirm>
      </div>
      <Table
        rowSelection={{
          type: "radio",
          ...rowSelection,
        }}
        columns={columns}
        dataSource={data}
      />
    </div>
  );
};

export default ManageGoodsReceiptComponents;
