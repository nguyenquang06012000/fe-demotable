import React, { useState } from "react";
import { Form, Icon, Input, Button, Checkbox, notification } from "antd";
import "./index.css";
import axios from "axios";
import { API } from "../../setting";

const NormalLoginForm = (props) => {
  const { setAuthor, setAuthen } = props;
  const handleSubmit = (e) => {
    e.preventDefault();
    props.form.validateFields((err, values) => {
      delete values.remember;
      console.log(values);
      if (!err) {
        axios.post(API + "/api/login", values).then((res) => {
          console.log(res.data)
          window.localStorage.setItem("username", res.data.username);
          setAuthor(res.data.username);
          window.localStorage.setItem("role", res.data.role.id);
          setAuthen(res.data.role.id);
          console.log(res.data.role.id)
          if (res.data.role.id === 1) {
            window.location.assign("http://localhost:3000/admin");
          }
          else {
            window.location.assign("http://localhost:3000/");
          }
        });

      }
    });
  };

  const { getFieldDecorator } = props.form;
  return (
    <Form onSubmit={handleSubmit} className="login-form">
      <Form.Item>
        {getFieldDecorator("username", {
          rules: [{ required: true, message: "Please input your username!" }],
        })(
          <Input
            prefix={<Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />}
            placeholder="Username"
          />
        )}
      </Form.Item>
      <Form.Item>
        {getFieldDecorator("password", {
          rules: [{ required: true, message: "Please input your Password!" }],
        })(
          <Input
            prefix={<Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />}
            type="password"
            placeholder="Password"
          />
        )}
      </Form.Item>
      <Form.Item>
        {getFieldDecorator("remember", {
          valuePropName: "checked",
          initialValue: true,
        })(<Checkbox>Remember me</Checkbox>)}
        <a className="login-form-forgot" href="">
          Forgot password
        </a>
        <Button
          type="primary"
          htmlType="submit"
          className="login-form-button"
          style={{ width: "100%" }}
        >
          Log in
        </Button>
        Or <a href="">register now!</a>
      </Form.Item>
    </Form>
  );
};

const FormPage = Form.create({ name: "normal_login" })(NormalLoginForm);
export default FormPage;
