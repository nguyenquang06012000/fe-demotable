import React from 'react';

function NavBar(props) {
    return (
        <div>
            <nav id="navigation">
                {/* container */}
                <div className="container">
                    {/* responsive-nav */}
                    <div id="responsive-nav">
                        {/* NAV */}
                        <ul className="main-nav nav navbar-nav">
                            <li className="active"><a href="/">Home</a></li>
                            <li><a href="/productAuctions">Product Aution</a></li>
                            <li><a href="/">CheckOut</a></li>
                        </ul>
                        {/* /NAV */}
                    </div>
                    {/* /responsive-nav */}
                </div>
                {/* /container */}
            </nav>
        </div>
    );
}

export default NavBar;