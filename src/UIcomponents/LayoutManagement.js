import React from 'react';
import SlideBarMenu from '../UIcomponents/viewManagement/SlideBarMenu';

const LayoutManagement = (props) => {
    return (
        <div className="hold-transition sidebar-mini layout-fixed">
            <div className="wrapper">
                {/* Navbar */}
                <nav className="main-header navbar navbar-expand navbar-white navbar-light">
                    {/* Left navbar links */}
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <a className="nav-link" data-widget="pushmenu" href="#" role="button"><i className="fas fa-bars" /></a>
                        </li>
                        <li className="nav-item d-none d-sm-inline-block">
                            <a href="/" className="nav-link">Home</a>
                        </li>
                    </ul>
                    {/* Right navbar links */}
                    
                </nav>
                {/* /.navbar */}
                {/* Main Sidebar Container */}
                <SlideBarMenu />
                {/* Content Wrapper. Contains page content */}

                <div className="content-wrapper">
                    {/* Main content */}
                    <section className="content">
                        {props.children}
                    </section>
                    {/* /.content */}
                </div>
                {/* /.content-wrapper */}
                {/* Control Sidebar */}
                <aside className="control-sidebar control-sidebar-dark">
                    {/* Control sidebar content goes here */}
                </aside>
                {/* /.control-sidebar */}
            </div>
            {/* ./wrapper */}
        </div>
    );
}

export default LayoutManagement;