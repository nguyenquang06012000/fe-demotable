import React from 'react';
import './index.css';
import ProducAuctionsComponents from '../../pages/admin/productsAuctions';

function AuctionManagement(props) {
    return (
        <div className="container-fluid">
            <div className="row">
                <div className="col-md-12 col-sm-12">
                    <div className="card">
                        <div className="card-header">
                            <h3><i className="fa fa-tag" aria-hidden="true" /> Products Auction</h3>
                        </div>
                        <div className="card-body">

                            <ProducAuctionsComponents />

                        </div>
                    </div>
                </div>
            </div>
        </div>


    );
}

export default AuctionManagement;