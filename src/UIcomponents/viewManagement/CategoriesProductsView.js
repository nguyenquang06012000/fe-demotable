import React from 'react';
import './index.css';
import ManageProductsComponents from '../../pages/admin/products/products';
import ManageCategoriesComponents from '../../pages/admin/category';

function ProductsManagement(props) {
    return (
        <div className="container-fluid">
            <div className="row">
                <div className="col-md-12 col-sm-12">
                    <div className="card">
                        <div className="card-header">
                            <h3><i className="fa fa-tag" aria-hidden="true"/> Categories</h3>
                        </div>
                        <div className="card-body">

                            <ManageCategoriesComponents />
                            
                        </div>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-md-12 col-sm-12">
                    <div className="card">
                        <div className="card-header">
                            <h3><i className="fa fa-tag" aria-hidden="true"/> Products</h3>
                        </div>
                        <div className="card-body">

                            <ManageProductsComponents />

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ProductsManagement;