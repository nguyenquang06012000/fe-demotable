import React from 'react';
import Header from '../UIcomponents/viewHome/Header';
import NavBar from '../UIcomponents/viewHome/NavBar';
import NewLetter from '../UIcomponents/viewHome/NewLetter';
import Footer from '../UIcomponents/viewHome/Footer';


const  Layout = (props) => {
    return (
        <div>
            <Header Author={props.Author} />
            <NavBar/>
            {props.children}
            <NewLetter/>
            <Footer/>
        </div>
    );
}

export default Layout;